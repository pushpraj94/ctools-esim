'use strict';

const SMDP = require('./smdp');
const extract = require('./extract');

module.exports = {
  extract,
  SMDP,
};
