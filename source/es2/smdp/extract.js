'use strict';

const config = require('./config');
const es2Response = require('./payloads').es2Response;

function buildES2Response(code, message, desc, smdpText, subjectCode, reasonCode) {
  const responsePayload = es2Response();
  responsePayload.code = code;
  responsePayload.message = message;
  responsePayload.desc = desc;
  responsePayload.smdpText = smdpText;
  responsePayload.subjectCode = subjectCode;
  responsePayload.reasonCode = reasonCode;
  return responsePayload;
}

/**
 * This function checks whether the ES2+ calls are successful or failed.
 * @param es2Result
 * @param statusMessage (message to be fetched based on the ES2+ call)
 * @returns statusObject
 */
function getES2Status(executionStatus, statusMessage) {
  if (executionStatus.status === config.es2Success) {
    const status = buildES2Response(config.statusCode.HTTP_200, executionStatus.status, config[`${statusMessage}Success`]);
    return Promise.resolve(status);
  } else if (executionStatus.status === config.es2Failure) {
    const es2ErrorCode = `${config.actionTypeId[`${statusMessage}Action`]}_${executionStatus.statusCodeData.subjectCode.replace(/\./g, '_')}_${executionStatus.statusCodeData.reasonCode.replace(/\./g, '_')}`;
    if (typeof config[es2ErrorCode] === 'undefined') {
      const status = buildES2Response(es2ErrorCode, executionStatus.status,
        config.unknownError, executionStatus.statusCodeData.message || config.unknownError,
        executionStatus.statusCodeData.subjectCode, executionStatus.statusCodeData.reasonCode);
      return Promise.reject(status);
    }
    const status = buildES2Response(es2ErrorCode, executionStatus.status, config[es2ErrorCode],
      executionStatus.statusCodeData.message, executionStatus.statusCodeData.subjectCode,
      executionStatus.statusCodeData.reasonCode);
    return Promise.reject(status);
  }
  const status = buildES2Response(config.errorCode.HTTP_701,
    executionStatus.status, config.unknownError, config.unknownError);
  return Promise.reject(status);
}

module.exports = {
  getES2Status,
};
