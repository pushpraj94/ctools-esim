'use strict';

function es2Response() {
  return {
    code: '',
    message: '',
    desc: '',
    smdpText: '',
    subjectCode: '',
    reasonCode: '',
  };
}

module.exports = {
  es2Response,
};
