'use strict';

const req = require('request');
const config = require('./config');
const debug = require('../../debug');

/**
 * SmdpClient object factory
 */
function SMDP(SMDPDetails, operator, user) {
  this.PRIVATE_ADDRESS = SMDPDetails.PRIVATE_SMDP_ADDRESS;
  this.PUBLIC_ADDRESS = SMDPDetails.PUBLIC_SMDP_FQDN;
  this.SMDP_NAME = SMDPDetails.SMDP_NAME;
  this.SMDS_ADDRESS = SMDPDetails.SMDS_ADDRESS;
  this.AUTHENTICATION_TYPE = SMDPDetails.AUTHENTICATION_TYPE;
  this.SSL_REQUIRED = SMDPDetails.SSL_REQUIRED;
  this.USER_NAME = SMDPDetails.USER_NAME;
  this.USER_PASSWORD = SMDPDetails.USER_PASSWORD;
  this.SMDP_CA = SMDPDetails.SMDP_CA;
  this.SMDP_CRT = SMDPDetails.SMDP_CRT;
  this.SMDP_KEY = SMDPDetails.SMDP_KEY;
  this.OPERATOR_ID = operator;
  this.USER = user;
  this.ES2_REQUESTER_IDENTIFIER = SMDPDetails.ES2_REQUESTER_IDENTIFIER;
  this.REQUEST_TIMEOUT = parseInt(SMDPDetails.REQUEST_TIMEOUT, config.decimalRadix);
}

SMDP.prototype.getSmdpUrl = function getSmdpUrl(urlPath) {
  debug('Inside getSmdpUrl function');
  let url;
  if (this.PRIVATE_ADDRESS) {
    url = `${this.PRIVATE_ADDRESS}${urlPath}`;
  } else {
    url = `${this.PUBLIC_ADDRESS}${urlPath}`;
  }
  return url;
};

/**
 * Function to make a ES2+ cancel order request
 * @param url (cancel order URL)
 * @param profileDetails which includes ICCID, EID and MATCHING_ID
 * @param ProfileStatusIndicator
 */
SMDP.prototype.cancelOrderCall = function cancelOrderCall(profileDetails,
  ProfileStatusIndicator) {
  debug(`Inside cancelOrderCall function :: profileDetails: ${profileDetails}`);
  const smdpUrlPath = `${config.cancelURL}`;
  const functionCallIdentifier = config.functionCallIdentifier1;
  this.smdpClientAddSSL(functionCallIdentifier);
  this.requestDetail.json.iccid = profileDetails.ICCID;
  if (profileDetails.EID) {
    this.requestDetail.json.eid = profileDetails.EID;
  }
  if (profileDetails.MATCHING_ID) {
    this.requestDetail.json.matchingId = profileDetails.MATCHING_ID;
  }
  this.requestDetail.json.finalProfileStatusIndicator = ProfileStatusIndicator;
  return this[`${this.AUTHENTICATION_TYPE}SmdpClientFactory`](smdpUrlPath);
};

/**
 * This function invokes SM-DP+ after implementing basic authentication
 * @param smdpUrlPath
 */
SMDP.prototype.basicAuthenticationSmdpClientFactory = function
 basicAuthenticationSmdpClientFactory(smdpUrlPath) {
  debug('Inside basicAuthenticationSmdpClientFactory');
  let urlPath = `${this.USER_NAME}:${this.USER_PASSWORD}@`;
  urlPath += this.getSmdpUrl(smdpUrlPath);
  debug(`Url path for basicAuthentication: ${urlPath}`);
  this.requestDetail.url = this.requestDetail.url.concat(urlPath);
  return this.callSMDP();
};

/**
 * This function invokes SM-DP+ without any authentication
 * @param smdpUrlPath
 */
SMDP.prototype.noAuthenticationSmdpClientFactory = function
 noAuthenticationSmdpClientFactory(smdpUrlPath) {
  debug('Inside noAuthenticationSmdpClientFactory function');
  const urlPath = this.getSmdpUrl(smdpUrlPath);
  debug(`Url path for noAuthentication: ${urlPath}`);
  this.requestDetail.url = this.requestDetail.url.concat(urlPath);
  return this.callSMDP();
};

/**
 * This function sets the required protocol to the request
 * and adds SSL certificate if SSL_REQUIRED is true
 * @param SMDPIdentifier
 */
SMDP.prototype.smdpClientAddSSL = function smdpClientAddSSL(SMDPIdentifier) {
  debug('Inside smdpClientAddSSL function');
  const json = {
    headers: {
      'Content-Type': config.appContentType,
      'X-Admin-Protocol': config.X_ADMIN_PROTOCOL,
    },
    timeout: this.REQUEST_TIMEOUT,
    json: {
      header: {
        functionRequesterIdentifier: this.ES2_REQUESTER_IDENTIFIER,
        functionCallIdentifier: SMDPIdentifier,
      },
    },
  };
  if (parseInt(this.SSL_REQUIRED, config.decimalRadix)) {
    if (this.SMDP_KEY) {
      json.key = this.SMDP_KEY;
    }
    if (this.SMDP_CRT) {
      json.cert = this.SMDP_CRT;
    }
    if (this.SMDP_CA) {
      json.ca = this.SMDP_CA;
    }
    json.url = config.HTTPS_PROTOCOL;
    json.rejectUnauthorized = false;
  } else {
    json.url = config.HTTP_PROTOCOL;
  }
  this.requestDetail = json;
};

/**
 * This function calls SM-DP+ for corresponding method
 * (downlaodOrder/confirmOrder/releaseProfile/cancelOrder)
 * @returns JSON object containing SM-DP+ response details
 */
SMDP.prototype.callSMDP = function callSMDP() {
  return new Promise((resolve, reject) => {
    req.post(this.requestDetail, (err, response, body) => {
      if (err || response.statusCode !== config.statusCode.HTTP_200) {
        return reject(this.validateES2Response(err, response));
      }
      return resolve(body);
    });
  });
};

/**
 * This function validates the error response from ES2+ calls.
 * @param error
 * @param response
 * @returns error status object
 */
SMDP.prototype.validateES2Response = function validateES2Response(error, response) {
  debug(`Inside validateES2Response function :: error : ${JSON.stringify(error)}`);
  if (error && (error.code === config.errorTimeout || error.code === config.errorSocket)) {
    return {
      code: config.requestTimeout,
      message: error.code,
      desc: config.requestTimedOutMessage,
      smdpText: config.requestTimedOutMessage,
      subjectCode: config.requestTimeout,
    };
  }
  return {
    code: config.serverUnreachable,
    message: error ? error.code : response.body,
    desc: config.smdpServerError,
    smdpText: config.smdpServerError,
    subjectCode: config.serverUnreachable,
  };
};

module.exports = SMDP;
