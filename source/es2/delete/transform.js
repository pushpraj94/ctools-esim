'use strict';

const config = require('./config');

function transform(req, res) {
  res.locals.iccid = req.params.id;
  res.locals.token = req.token;
  res.locals.profileStatusIndicator = req.query.finalProfileStatusIndicator || config.unavailable;
}

module.exports = {
  transform,
};
