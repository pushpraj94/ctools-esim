'use strict';

const isValid = require('./validate.js').isValid;
const transform = require('./transform.js').transform;

module.exports = {
  isValid,
  transform,
};
