'use strict';

module.exports = {
  profileStatusIndicator: ['Available', 'Unavailable'],
  regexIccid: /^[1-9][0-9]{18}[0-9F]?$/,
  unavailable: 'Unavailable',
};
