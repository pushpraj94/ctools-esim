'use strict';

const config = require('./config');
const debug = require('../../debug');
const Joi = require('joi');
const _ = require('lodash');

const statusList = config.profileStatusIndicator;

const schema = {
  params: {
    id: Joi.string().regex(config.regexIccid).required().label('ICCID'),
  },
  query: {
    finalProfileStatusIndicator: Joi.string().valid(statusList).allow(''),
  },
};

function validateSchema(obj) {
  const clone = _.cloneDeep(obj);
  const out = Joi.validate(clone, schema);
  debug('Cancel order call validateSchema errors :: ', out.error ? out.error.details : null);
  if (!out.error) {
    return false;
  }
  return out.error.details[0].message;
}

function isValid(obj) {
  return validateSchema(obj);
}

module.exports = {
  isValid,
};
