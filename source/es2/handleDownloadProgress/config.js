'use strict';

module.exports = {
  ALLOWED_VALUES: {
    POINT_STATUS: {
      SUCCESS: 'Executed-Success',
      WARNING: 'Executed-WithWarning',
      FAILED: 'Failed',
    },
    POINT_ID: {
      ELIGIBILITY_CHECK: {
        KEY: '1',
        ERROR: {
          '8.1.1': ['3.8'],
          '8.2.5': ['4.3'],
          '8.8.5': ['6.4', '3.8'],
          '8.2.8': ['1.2'],
          '8.2.9': ['3.11'],
          '1.2': ['4.2'],
        },
      },
      CONFIRMATION_CODE_CHECK: {
        KEY: '2',
        ERROR: {
          '8.2.7': ['2.2', '3.8', '6.4'],
        },
      },
      BPP_DOWNLOAD: {
        KEY: '3',
        ERROR: {
          '8.2.10': ['4.2'],
          '8.10.2': ['6.4', '3.8'],
        },
      },
      BPP_INSTALLATION: {
        KEY: '4',
        ERROR: {
          '8.2.10': ['2', '3', '4', '5', '6'],
        },
      },
      PROFILE_ENABLED: {
        KEY: '101',
        ERROR: {},
      },
      PROFILE_DISABLED: {
        KEY: '102',
        ERROR: {},
      },
      PROFILE_DELETED: {
        KEY: '103',
        ERROR: {},
      },
    },
  },
  paths: {
    requesterId: {
      path: 'header.functionRequesterIdentifier',
      type: 'string',
    },
    functionCallIdentifier: {
      path: 'header.functionCallIdentifier',
      type: 'string',
    },
    iccid: {
      path: 'iccid',
      type: 'string',
    },
    eid: {
      path: 'eid',
      type: 'string',
    },
    profileType: {
      path: 'profileType',
      type: 'string',
    },
    timestamp: {
      path: 'timestamp',
      type: 'string',
    },
    notificationPointId: {
      path: 'notificationPointId',
      type: 'string',
    },
    notificationPointStatus: {
      path: 'notificationPointStatus.status',
      type: 'string',
    },
    resultData: {
      path: 'resultData',
      type: 'string',
    },
    subjectCode: {
      path: 'notificationPointStatus.statusCodeData.subjectCode',
      type: 'string',
    },
    reasonCode: {
      path: 'notificationPointStatus.statusCodeData.reasonCode',
      type: 'string',
    },
    subjectIdentifier: {
      path: 'notificationPointStatus.statusCodeData.subjectIdentifier',
      type: 'string',
    },
    message: {
      path: 'notificationPointStatus.statusCodeData.message',
      type: 'string',
    },
  },
};
