'use strict';

const op = require('object-path');

function extractRequestData(body) {
  return {
    requesterId: op.get(body, 'header.functionRequesterIdentifier'),
    functionCallIdentifier: op.get(body, 'header.functionCallIdentifier'),
    iccid: op.get(body, 'iccid'),
    eid: op.get(body, 'eid'),
    profileType: op.get(body, 'profileType'),
    timestamp: op.get(body, 'timestamp'),
    notificationPointId: op.get(body, 'notificationPointId'),
    notificationPointStatus: op.get(body, 'notificationPointStatus.status'),
    resultData: op.get(body, 'resultData'),
    subjectCode: op.get(body, 'notificationPointStatus.statusCodeData.subjectCode'),
    reasonCode: op.get(body, 'notificationPointStatus.statusCodeData.reasonCode'),
    subjectIdentifier: op.get(body, 'notificationPointStatus.statusCodeData.subjectIdentifier'),
    message: op.get(body, 'notificationPointStatus.statusCodeData.message'),
  };
}

module.exports = {
  extractRequestData,
};
