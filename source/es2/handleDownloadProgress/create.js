'use strict';

const _ = require('lodash');
const op = require('object-path');
const es2Paths = require('./config').paths;

function create(data) {
  const out = {};

  _.each(data, (val, key) => {
    if (es2Paths[key] && val !== undefined) {
      op.set(
        out,
        es2Paths[key].path,
        es2Paths[key].type === 'string' ? val.toString() : val
      );
    }
  });

  return out;
}

module.exports = {
  create,
};
