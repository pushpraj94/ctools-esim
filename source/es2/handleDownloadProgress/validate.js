'use strict';

const Joi = require('joi');
const _ = require('lodash');
const ALLOWED_VALUES = require('./config.js').ALLOWED_VALUES;
const extractRequestData = require('./extract.js').extractRequestData;
const debug = require('../../debug');

const schema = {
  header: Joi.object().required().keys({
    functionRequesterIdentifier: Joi.string().required(),
    functionCallIdentifier: Joi.string().required().valid('Notification'),
  }),
  eid: Joi.string().optional().regex(/^[0-9]{32}$/),
  iccid: Joi.string().required().regex(/^[1-9][0-9]{18}[0-9F]?$/),
  profileType: Joi.string().required(),
  timestamp: Joi.string().required()
    .regex(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[T,D,Z]{1}$/),
  notificationPointId: Joi.string().required().valid(_.map(ALLOWED_VALUES.POINT_ID, x => (x.KEY))),
  notificationPointStatus: Joi.object().required().keys({
    status: Joi.string().required().valid(_.map(ALLOWED_VALUES.POINT_STATUS, x => (x))),
    statusCodeData: Joi.object().keys({
      subjectCode: Joi.string().required().allow(''),
      reasonCode: Joi.string().required().allow(''),
      subjectIdentifier: Joi.string().optional().allow(''),
      message: Joi.string().optional().allow(''),
    }),
  }),
  resultData: Joi.string().optional(),
};

function validateSchema(obj) {
  const x = _.cloneDeep(obj);
  x.notificationPointId += '';
  const out = Joi.validate(x, schema);
  debug('ES2 handleDownloadProgress validateSchema errors:', out.error ? out.error.details : null);
  return !out.error;
}

function validateErrorCodes(obj) {
  const FAILED = ALLOWED_VALUES.POINT_STATUS.FAILED;
  const data = extractRequestData(obj);
  const pointId = data.notificationPointId;
  const status = data.notificationPointStatus;

  if (status === FAILED) {
    const allowed = _.filter(ALLOWED_VALUES.POINT_ID, x => (x.KEY === pointId))[0].ERROR;
    if (!allowed[data.subjectCode] || allowed[data.subjectCode].indexOf(data.reasonCode) === -1) {
      return false;
    }
  } else if (
    (data.subjectCode !== '' && data.subjectCode !== undefined)
    || (data.reasonCode !== '' && data.reasonCode !== undefined)
  ) {
    return false;
  }

  return true;
}

function isValid(obj) {
  return validateSchema(obj) && validateErrorCodes(obj);
}

module.exports = {
  isValid,
};
