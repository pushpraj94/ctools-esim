'use strict';

const Joi = require('joi');
const _ = require('lodash');
const debug = require('../../debug');
const config = require('./config');

const iccidSchema = {
  id: Joi.string().regex(config.iccidRegex).required().label('ICCID'),
};

const msisdnSchema = {
  msisdn: Joi.string().required().label('MSISDN'),
};

const validateIccidSchema = (obj) => {
  debug(`Validation parameter is: ${JSON.stringify(obj)}`);
  const clone = _.cloneDeep(obj);
  const out = Joi.validate(clone, iccidSchema);
  debug('Validation error: ', out.error ? out.error.details : null);
  return out.error ? out.error.details : null;
};

const validateMsisdnSchema = (obj) => {
  debug(`Validation parameter is: ${JSON.stringify(obj)}`);
  const clone = _.cloneDeep(obj);
  const out = Joi.validate(clone, msisdnSchema);
  debug('Validation error: ', out.error ? out.error.details : null);
  return out.error ? out.error.details : null;
};

module.exports = {
  validateIccidSchema,
  validateMsisdnSchema,
};
