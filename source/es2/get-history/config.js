'use strict';

module.exports = {
  iccidRegex: /^[1-9][0-9]{18}[0-9F]?$/,
};
