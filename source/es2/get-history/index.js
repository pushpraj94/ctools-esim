'use strict';

const validateIccidSchema = require('./validation').validateIccidSchema;
const validateMsisdnSchema = require('./validation').validateMsisdnSchema;

module.exports = {
  validateIccidSchema,
  validateMsisdnSchema,
};
