'use strict';

const config = {
  REGEX: {
    DUOBILL: /^[a-zA-Z]$/,
    UID: /^[1-9]{1}[0-9]{14}$/,
    ICCID: /^[1-9][0-9]{18}[0-9F]?$/,
    IMSI: /^[1-9]{1}[0-9]{14}$/,
    ACC: /^[0-9]{4}$/,
    PUK: /^[0-9]{4,8}$/,
    PIN: /^[0-9]{4,8}$/,
    STRING: /^[A-Za-z0-9][A-Za-z0-9_-]*$/,
  },
  MESSAGES: {
    INVALID_DUOBILL: 'INVALID_DUOBILL',
    INVALID_UID: 'INVALID_UID',
    INVALID_ICCID: 'INVALID_ICCID',
    INVALID_IMSI: 'INVALID_IMSI',
    INVALID_ACC: 'INVALID_ACC',
    INVALID_PUK1: 'INVALID_PUK1',
    INVALID_PUK2: 'INVALID_PUK2',
    INVALID_PIN1: 'INVALID_PIN1',
    INVALID_PIN2: 'INVALID_PIN2',
    INVALID_PROFILE_TYPE: 'INVALID_PROFILE_TYPE',
    INVALID_SUPPLIER: 'INVALID_SUPPLIER',
    INVALID_SMDP: 'INVALID_SMDP',
    INVALID_FILE_FORMAT: 'INVALID_FILE_FORMAT',
    INVALID_FILE_NAME: 'INVALID_FILE_NAME',
    SUCCESSFUL_UPLOAD: 'File submitted for processing',
  },
  paths: {
    uploadId: {
      path: 'ids.0.items.$',
      type: 'string',
    },
    desc: {
      path: 'desc',
      type: 'string',
    },
    errorCode: {
      path: 'error-codes.0.items.$ref',
      type: 'string',
    },
    descriptions: {
      path: 'descriptions.0.items.$',
      type: 'string',
    },
    timestamp: {
      path: 'timestamp',
      type: 'string',
    },
  }
};

module.exports = config;
