'use strict';

const debug = require('../debug');
const op = require('object-path');
const { create } = require('./create');
const config = require('./config');

exports.success = function success(req, res, next) {
  debug('File upload success');
  const obj = {
    uploadId: res.locals.uploadID,
    desc: config.MESSAGES.SUCCESSFUL_UPLOAD,
  }
  res.status(201).send(create(obj));
  return next();
};

exports.error = function error(err, req, res, next) {
  debug('File upload failed');
  debug(JSON.stringify(res.locals.error));
  if (err !== 'SYSTEM_ERROR') {
    res.status(404).send(create(res.locals.error));
    return next();
  }
  res.status(500).send(create(res.locals.error));
  return next();
};
