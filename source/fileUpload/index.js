'use strict';

const validate = require('./validate').validateFile;
const handlers = require('./handlers');

module.exports = {
  validate,
  handlers,
}
