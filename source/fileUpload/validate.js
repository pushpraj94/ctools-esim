'use strict';

const path = require('path');
const config = require('./config');
const debug = require('../debug');
const schema = require('./schema').getSchema();
const _ = require('lodash');
const Joi = require('joi');

function validateSchema(obj) {
  const x = _.cloneDeep(obj);
  const out = Joi.validate(x, schema, { allowUnknown: true });
  const result = { valid: true };
  debug('SIM validateSchema errors:', out.error ? out.error.details : null);
  if (out.error) {
    result.valid = false;
    result.message = config.MESSAGES[`INVALID_${out.error.details[0].context.key}`];
  }
  return result;
}

function validateFile(files) {
  return new Promise((resolve, reject) => {
    debug(`Validating File name, extension and content - ${JSON.stringify(files)}`);
    if (!files || path.extname(files.originalFilename) !== '.json') {
      debug('Invalid File Format');
      return reject(config.MESSAGES.INVALID_FILE_FORMAT);
    }
    const pattern = config.REGEX.STRING;
    const fileBaseName = path.basename(files.originalFilename, '.json');
    debug(`Base name of the file - ${fileBaseName}`);
    if (!pattern.test(fileBaseName)) {
      debug('Invalid file name');
      return reject(config.MESSAGES.INVALID_FILE_NAME);
    }
    const getFilecontent = require; // to avoid the eslint error
    const result = validateSchema(getFilecontent(files.path));
    if (!result.valid) {
      return reject(result.message);
    }
    return resolve();
  });
}


module.exports = {
  validateFile,
};
