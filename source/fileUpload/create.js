'use strict';

const _ = require('lodash');
const op = require('object-path');
const paths = require('./config').paths;

function create(data) {
  const out = {};

  _.each(data, (val, key) => {
    if (paths[key] && val !== undefined) {
      op.set(
        out,
        paths[key].path,
        paths[key].type === 'string' ? val.toString() : val
      );
    }
  });

  return out;
}

module.exports = {
  create,
};
