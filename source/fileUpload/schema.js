'use strict';

const Joi = require('joi');
const config = require('./config');

function getSchema() {
  const schema = Joi.array().items(Joi.object().keys({
    Duobill: Joi.string().regex(config.REGEX.DUOBILL).label('DUOBILL'),
    UID: Joi.string().regex(config.REGEX.UID).label('UID'),
    'ICC-ID': Joi.string().regex(config.REGEX.ICCID).label('ICCID'),
    IMSI: Joi.string().regex(config.REGEX.IMSI).label('IMSI'),
    ACC: Joi.string().regex(config.REGEX.ACC).label('ACC'),
    PUK: Joi.string().regex(config.REGEX.PUK).label('PUK1'),
    PIN: Joi.string().regex(config.REGEX.PIN).label('PIN1'),
    PUK2: Joi.string().regex(config.REGEX.PUK).label('PUK2'),
    PIN2: Joi.string().regex(config.REGEX.PIN).label('PIN2'),
    'Prof-Type': Joi.string().regex(config.REGEX.STRING).label('PROFILE_TYPE'),
    Supplier: Joi.string().regex(config.REGEX.STRING).label('SUPPLIER'),
    SMDP: Joi.string().hostname().label('SMDP'),
  }));

  return schema;
}

module.exports = {
  getSchema,
};
