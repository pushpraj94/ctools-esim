'use strict';

const handleDownloadProgress = require('../es2').handleDownloadProgress;
const nap = require('../nap');

function handleDownloadProgressRequest2nap(data) {
  const obj = handleDownloadProgress.extractRequestData(data);
  const mapping = {
    iccid: obj.iccid,
    eid: obj.eid,
    downloadResponseStatus: obj.notificationPointStatus,
    downloadResponseDate: obj.timestamp,
    downloadRequestStatus: obj.notificationPointId,
    downloadResponseDescription: obj.resultData,
    providerPreferedName: obj.requesterId,
    description: obj.message,
  };

  if (obj.subjectCode && obj.reasonCode) {
    mapping.downloadResponseCode = `${obj.subjectCode}.${obj.reasonCode}`;
  }

  return nap.create(mapping);
}

module.exports = {
  handleDownloadProgressRequest2nap,
};
