'use strict';

const _ = require('lodash');
const op = require('object-path');
const napPaths = require('./config');

function create(data) {
  const nap = {};

  _.each(data, (val, key) => {
    if (napPaths[key] && val !== undefined) {
      op.set(
        nap,
        napPaths[key].path,
        napPaths[key].type === 'string' ? val.toString() : val
      );
    }
  });

  return nap;
}

module.exports = {
  create,
};
