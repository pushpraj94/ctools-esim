'use strict';

const _ = require('lodash');
const op = require('object-path');
const napPaths = require('./config');

function extractData(nap) {
  const out = {};
  _.each(napPaths, (data, key) => {
    const value = op.get(nap, data.path);
    if (value) {
      out[key] = value;
    }
  });
  return out;
}

module.exports = {
  extractData,
};
