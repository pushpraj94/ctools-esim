'use strict';

const validate = require('./validate.js').validate;
const isValid = require('./validate.js').isValid;
const create = require('./create.js').create;
const extractData = require('./extract.js').extractData;
const handlers = require('./handlers');

module.exports = {
  create,
  extractData,
  isValid,
  validate,
  handlers,
};
