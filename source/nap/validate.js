'use strict';

const validator = require('is-my-json-valid');
const schema = require('./schema.json');

const napVal = validator(schema);

function validate(nap) {
  napVal(nap);
  return napVal.errors;
}

function isValid(nap) {
  return !validate(nap);
}

module.exports = {
  isValid,
  validate,
};
