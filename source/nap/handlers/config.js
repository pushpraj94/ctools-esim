'use strict';

module.exports = {
  CSMErrorContentType: 'application/vnd.vodafone.fault.v1+json',
  CSMSuccessContentType: 'application/vnd.vodafone.network-access-profile.v1+json',
  dbFailureMsg: 'DB Operation failed',
  errorCode: {
    400: 101,
    404: 491,
    491: 491,
    500: 500,
    701: 701,
    702: 702,
    108: 108,
  },
  invalidSearchType: 'SearchType Invalid',
  invalidSMDPId: 'Unknown SM-DP ID',
  smdpRequestFailed: 'SM-DP+ operation failure',
  statusCode: {
    HTTP_200: 200,
    HTTP_201: 201,
    HTTP_204: 204,
    HTTP_400: 400,
    HTTP_401: 401,
    HTTP_403: 403,
    HTTP_404: 404,
    HTTP_409: 409,
    HTTP_491: 491,
    HTTP_500: 500,
  },
  unknownICCID: 'Unknown ICCID',
};
