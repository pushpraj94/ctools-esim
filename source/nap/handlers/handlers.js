'use strict';

const op = require('object-path');
const errorJson = require('./payloads').errorJson;
const debug = require('../../debug');
const config = require('./config');

const ERROR_VALIDATION = 'ERROR_VALIDATION';
const SYSTEM_ERROR = 'SYSTEM_ERROR';
const NO_DATA = 'NO_DATA';
const NO_ICCID_DATA = 'NO_ICCID_DATA';
const INVALID_SMDP_ID = 'INVALID_SMDP_ID';

function buildErrorPayload(errorCode, timestamp, err) {
  const payload = errorJson();
  payload['error-codes'][0].$ = config.errorCode[errorCode].toString();
  payload.timestamp = timestamp;
  if (op.get(err, 'error')) {
    payload.descriptions[0].$ = err.error;
  }
  if (op.get(err, 'failures')) {
    if (err.error === config.invalidSMDPId) {
      payload.descriptions[0].$ = config.invalidSMDPId;
    } else {
      payload.descriptions[0].$ = config.smdpRequestFailed;
    }
    payload.failures = err.failures;
  }
  return payload;
}

const error = (err, req, res, next) => {
  debug(res.getHeader('X-Request-ID'), `\tError - ${err}`);
  res.setHeader('Content-type', config.CSMErrorContentType);
  if (err === ERROR_VALIDATION) {
    const errorPayload = buildErrorPayload(config.statusCode.HTTP_400, res.locals.timestamp);
    errorPayload.descriptions[0].$ = res.locals.err[0].message;
    res.status(config.statusCode.HTTP_400).send(errorPayload);
    return next();
  } else if (err === NO_DATA) {
    res.setHeader('X-ResultStatus-ErrorCode', config.statusCode.HTTP_491);
    const errorPayload = buildErrorPayload(config.statusCode.HTTP_404, res.locals.timestamp);
    errorPayload.descriptions[0].$ = config.invalidSearchType.replace(/SearchType/g, res.locals.searchType);
    res.status(config.statusCode.HTTP_404).send(errorPayload);
    return next();
  } else if (err === SYSTEM_ERROR) {
    const errorPayload = buildErrorPayload(config.statusCode.HTTP_500, res.locals.timestamp);
    errorPayload.descriptions[0].$ = config.dbFailureMsg;
    res.status(config.statusCode.HTTP_500).send(errorPayload);
    return next();
  } else if (err === NO_ICCID_DATA) {
    res.status(config.statusCode.HTTP_404).send(buildErrorPayload(config.statusCode.HTTP_404, res.locals.timestamp, {
      status: config.statusCode.HTTP_404,
      error: config.unknownICCID,
    }));
    return next();
  } else if (err === INVALID_SMDP_ID) {
    res.status(config.statusCode.HTTP_404).send(buildErrorPayload(config.statusCode.HTTP_404, res.locals.timestamp, {
      status: config.statusCode.HTTP_404,
      error: config.invalidSMDPId,
    }));
    return next();
  } else if (err.status === config.statusCode.HTTP_400) {
    res.status(config.statusCode.HTTP_400).send(buildErrorPayload(config.statusCode.HTTP_400, res.locals.timestamp, err));
    return next();
  } else if (err.status === config.statusCode.HTTP_404) {
    res.setHeader('X-ResultStatus-ErrorCode', config.statusCode.HTTP_491);
    res.status(config.statusCode.HTTP_404).send(buildErrorPayload(config.statusCode.HTTP_404, res.locals.timestamp, err));
    return next();
  } else if (err.status === config.statusCode.HTTP_409) {
    const errorCode = err.errrorCode || config.statusCode.HTTP_404;
    res.status(config.statusCode.HTTP_409)
    .send(buildErrorPayload(errorCode, res.locals.timestamp, err));
    return next();
  }
  res.status(config.statusCode.HTTP_404).send(err);
  return next();
};

const searchSuccess = (req, res, next) => {
  debug(res.getHeader('X-Request-ID'), '\tMW: Sending search result');
  res.setHeader('Content-type', config.CSMSuccessContentType);
  res.status(config.statusCode.HTTP_200).send(res.locals.searchPayload);
  return next();
};

function cancelSuccess(req, res) {
  res.setHeader('Content-type', config.CSMSuccessContentType);
  res.sendStatus(config.statusCode.HTTP_204);
  res.end();
}

module.exports = {
  error,
  searchSuccess,
  cancelSuccess,
};
