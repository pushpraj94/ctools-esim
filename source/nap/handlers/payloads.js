'use strict';

function errorJson() {
  return {
    timestamp: '',
    'error-codes': [{
      $: '',
    }],
    descriptions: [{
      $: '',
    }],
  };
}

module.exports = {
  errorJson,
};
