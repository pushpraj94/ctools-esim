'use strict';

module.exports = {
  iccid: {
    path: 'ids.0.$',
    type: 'string',
  },
  type: {
    path: 'type.$',
    type: 'string',
  },
  status: {
    path: 'status.$',
    type: 'string',
  },
  description: {
    path: 'desc',
    type: 'string',
  },
  eid: {
    path: 'parts.logical-resource.eid',
    type: 'string',
  },
  msisdn: {
    path: 'parts.logical-resource.msisdn',
    type: 'string',
  },
  imei: {
    path: 'parts.physical-resource.imei',
    type: 'string',
  },
  provider: {
    path: 'roles.provider.name',
    type: 'string',
  },
  pin1: {
    path: 'details.pin1',
    type: 'string',
  },
  pin2: {
    path: 'details.pin2',
    type: 'string',
  },
  puk1: {
    path: 'details.puk1',
    type: 'string',
  },
  puk2: {
    path: 'details.puk2',
    type: 'string',
  },
  matchingId: {
    path: 'details.matching-id',
    type: 'string',
  },
  confirmationCode: {
    path: 'details.confirmation-code.$',
    type: 'string',
  },
  confirmationCodeRequired: {
    path: 'details.confirmation-code-required',
    type: 'boolean',
  },
  smdpId: {
    path: 'details.smdp-id',
    type: 'string',
  },
  smdpFqdn: {
    path: 'details.smdpfqdn',
    type: 'string',
  },
  downloadResponseStatus: {
    path: 'parts.download-requests.0.download-request.download-response-status.$',
    type: 'string',
  },
  downloadResponseDate: {
    path: 'parts.download-requests.0.download-request.download-response-date',
    type: 'string',
  },
  downloadResponseCode: {
    path: 'parts.download-requests.0.download-request.download-response-code.$',
    type: 'string',
  },
  downloadRequestStatus: {
    path: 'parts.download-requests.0.download-request.status.$',
    type: 'string',
  },
  downloadResponseDescription: {
    path: 'parts.download-requests.0.download-request.download-response-description',
    type: 'string',
  },
  providerPreferedName: {
    path: 'roles.provider.individual.individual-name.preferred-given-name',
    type: 'string',
  },
};
