'use strict';

const es2 = require('./source/es2');
const nap = require('./source/nap');
const transform = require('./source/transform');
const fileUpload = require('./source/fileUpload');

module.exports = {
  es2,
  nap,
  transform,
  fileUpload,
};
