'use strict';

const expect = require('chai').expect;
const create = require('../../').nap.create;

const iccid = 'some_iccid_value';
const type = 'some_type_value';
const status = 'some_status_value';
const description = 'some_description_value';
const eid = 'some_eid_value';
const msisdn = 'some_msisdn_value';
const imei = 'some_imei_value';
const provider = 'some_provider_value';
const pin1 = 'some_pin1_value';
const pin2 = 'some_pin2_value';
const puk1 = 'some_puk1_value';
const puk2 = 'some_puk2_value';
const matchingId = 'some_matchingId_value';
const confirmationCode = 'some_confirmationCode_value';
const confirmationCodeRequired = 'some_confirmationCodeRequired_value';
const smdpId = 'some_smdpId_value';
const downloadResponseStatus = 'some_downloadResponseStatus_value';
const downloadResponseDate = 'some_downloadResponseDate_value';
const downloadResponseCode = 'some_downloadResponseCode_value';
const downloadRequestStatus = 'some_downloadRequestStatus_value';
const downloadResponseDescription = 'some_downloadResponseDescription_value';
const providerPreferedName = 'some_providerPreferedName_value';

const nap = {
  ids: [
    {
      $: iccid,
    },
  ],
  type: {
    $: type,
  },
  status: {
    $: status,
  },
  desc: description,
  parts: {
    'logical-resource': {
      eid,
      msisdn,
    },
    'physical-resource': {
      imei,
    },
    'download-requests': [
      {
        'download-request': {
          'download-response-status': {
            $: downloadResponseStatus,
          },
          'download-response-date': downloadResponseDate,
          'download-response-code': {
            $: downloadResponseCode,
          },
          'status': {
            $: downloadRequestStatus,
          },
          'download-response-description': downloadResponseDescription,
        },
      },
    ],
  },
  roles: {
    provider: {
      name: provider,
      individual: {
        'individual-name': {
          'preferred-given-name': providerPreferedName,
        },
      },
    },
  },
  details: {
    pin1,
    pin2,
    puk1,
    puk2,
    'matching-id': matchingId,
    'confirmation-code': {
      $: confirmationCode,
    },
    'confirmation-code-required': confirmationCodeRequired,
    'smdp-id': smdpId,
  },
};

describe('Network Access Profile create request test suite', function() {
  it('has an create function', function() {
    expect(create).to.be.a('function');
  });

  it('creates a network access profile successfully', function() {
    const data = create({
      iccid,
      type,
      status,
      description,
      eid,
      msisdn,
      imei,
      provider,
      pin1,
      pin2,
      puk1,
      puk2,
      matchingId,
      confirmationCode,
      confirmationCodeRequired,
      smdpId,
      downloadResponseStatus,
      downloadResponseDate,
      downloadResponseCode,
      downloadRequestStatus,
      downloadResponseDescription,
      providerPreferedName,
    });
    expect(data).to.be.eql(nap);
  });
});
