'use strict';

const expect = require('chai').expect;
const isValid = require('../../').nap.isValid;

describe('Network Access Profile validation test suite', function() {
  it('has an isValid function', function() {
    expect(isValid).to.be.a('function');
  });
});
