'use strict';

const mocks = require('node-mocks-http');
const expect = require('chai').expect;
const handlers = require('../../source/nap/handlers');

let req;
let res;

beforeEach(function() {
  req = mocks.createRequest();
  res = mocks.createResponse();
  res.locals = { timestamp: '', searchPayload: {}, err: [{ message: '' }]}
});


describe('handlers test suite', function() {
  describe('cancelSuccess UTC', function() {
    it('has cancelSuccess function', function() {
      expect(handlers.cancelSuccess).to.be.a('function');
    });

    it('searchSuccess function - should return 200 status with blank payload', function() {
      res.locals = {};
      handlers.cancelSuccess(req, res, () => {
        expect(res.statusCode).to.equal(204);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.network-access-profile.v1+json' });
      });
    });
   });

  describe('searchSuccess UTC', function() {
    it('has searchSuccess function', function() {
      expect(handlers.searchSuccess).to.be.a('function');
    });
  
    it('searchSuccess function - should return 200 status with blank payload', function() {
      res.locals = {};
      res.locals.searchPayload = {};
      handlers.searchSuccess(req, res, () => {
        expect(res.statusCode).to.equal(200);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.network-access-profile.v1+json' });
        expect(res.locals.searchPayload).to.deep.equal({});
      });
    });
  
    it('searchSuccess function - should return 200 status with payload', function() {
      res.locals = {};
      res.locals.searchPayload = { message: 'success'};
      handlers.searchSuccess(req, res, () => {
        expect(res.statusCode).to.equal(200);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.network-access-profile.v1+json' });
        expect(res.locals.searchPayload).to.deep.equal({ message: 'success'});
      });
    });
  });

  describe('error UTC', function() {
    it('has error function', function() {
      expect(handlers.errorHandler).to.be.a('function');
    });
  
    it('validation error - should return 400 status', function() {
      res.locals.timestamp = '';
      res.locals.err[0].message = 'validation failure';
      const ERROR_VALIDATION = 'ERROR_VALIDATION';
      handlers.errorHandler(ERROR_VALIDATION, req, res, () => {
        expect(res.statusCode).to.equal(400);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.fault.v1+json' });
      });
    });

    it('no result found for given query - should return 404 status', function() {
      res.locals.timestamp = '';
      res.locals.err[0].message = 'not found';
      const NO_DATA = 'NO_DATA';
      handlers.errorHandler(NO_DATA, req, res, () => {
        expect(res.statusCode).to.equal(404);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.fault.v1+json', 'X-ResultStatus-ErrorCode': 491});
      });
    });

    
    it('System error - should return 500 status', function() {
      res.locals.timestamp = '';
      res.locals.err[0].message = 'system error';
      const ERROR_VALIDATION = 'SYSTEM_ERROR';
      handlers.errorHandler(ERROR_VALIDATION, req, res, () => {
        expect(res.statusCode).to.equal(500);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.fault.v1+json' });
      });
    });

    it('INVALID_SMDP_ID - should return 404 status', function() {
      res.locals.timestamp = '';
      res.locals.err[0].message = 'invalid smdp id';
      const ERROR_VALIDATION = 'INVALID_SMDP_ID';
      handlers.errorHandler(ERROR_VALIDATION, req, res, () => {
        expect(res.statusCode).to.equal(404);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.fault.v1+json' });
      });
    });

    it('CONFLICT state - should return 409 status', function() {
      res.locals.timestamp = '';
      res.locals.err[0].message = 'conflict';
      const err = { status: 409 };
      handlers.errorHandler(err, req, res, () => {
        expect(res.statusCode).to.equal(409);
        expect(res._getHeaders('Content-type')).to.deep.equal({ 'Content-type': 'application/vnd.vodafone.fault.v1+json' });
      });
    });
  });
});