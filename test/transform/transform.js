'use strict';

const expect = require('chai').expect;
const handleDownloadProgressRequest2nap = require('../../').transform.handleDownloadProgressRequest2nap;
const _ = require('lodash');

const requesterId = 'some_requesterId_value';
const functionCallIdentifier = 'some_functionCallIdentifier_value';
const eid = 'some_eid_value';
const iccid = 'some_iccid_value';
const profileType = 'some_profileType_value';
const timestamp = 'some_timestamp_value';
const notificationPointId = 'some_notificationPointId_value';
const notificationPointStatus = 'some_notificationPointStatus_value';
const resultData = 'some_resultData_value';
const subjectCode = 'some_subjectCode_value';
const reasonCode = 'some_reasonCode_value';
const subjectIdentifier = 'some_subjectIdentifier_value';
const message = 'some_message_value';

const es2Req = {
  header: {
    functionRequesterIdentifier: requesterId,
    functionCallIdentifier: functionCallIdentifier,
  },
  eid,
  iccid,
  profileType,
  timestamp,
  notificationPointId,
  notificationPointStatus: {
    status: notificationPointStatus,
    statusCodeData: {
      subjectCode: subjectCode,
      reasonCode: reasonCode,
      subjectIdentifier: subjectIdentifier,
      message: message,
    },
  },
  resultData,
};

const nap = {
  ids: [
    {
      $: iccid,
    },
  ],
  desc: message,
  parts: {
    'logical-resource': {
      eid,
    },
    'download-requests': [
      {
        'download-request': {
          'download-response-status': {
            $: notificationPointStatus,
          },
          'download-response-date': timestamp,
          'download-response-code': {
            $: `${subjectCode}.${reasonCode}`,
          },
          'status': {
            $: notificationPointId,
          },
          'download-response-description': resultData,
        },
      },
    ],
  },
  roles: {
    provider: {
      individual: {
        'individual-name': {
          'preferred-given-name': requesterId,
        },
      },
    },
  },
};

describe('Transform handleDownloadProgressRequest2nap test suite', function() {
  it('has an handleDownloadProgressRequest2nap function', function() {
    expect(handleDownloadProgressRequest2nap).to.be.a('function');
  });

  it('transforms successfully', function() {
    const tr = handleDownloadProgressRequest2nap(es2Req);
    expect(tr).to.be.eql(nap);
  });

  it('if subjectCode and reasonCode are undefined then download-response-code should be undefined', function() {
    const es2ReqTemp = _.cloneDeep(es2Req);
    delete es2ReqTemp.notificationPointStatus.statusCodeData.subjectCode;
    delete es2ReqTemp.notificationPointStatus.statusCodeData.reasonCode;
    const tr = handleDownloadProgressRequest2nap(es2ReqTemp);

    const nap2 = _.cloneDeep(nap);
    delete nap2.parts['download-requests'][0]['download-request']['download-response-code'];
    expect(tr).to.be.eql(nap2);
  });
});
