'use strict';

const expect = require('chai').expect;
const validate = require('../../source/es2/delete/validate').isValid;
const validatePayload = require('./payloads').validatePayload;

const incorrectICCID = "12345";
const correctICCID = "12345678901234567890";
const correctICCIDF = "1234567890123456789F";
const ICCID = "1234567890123456789";
const unavailableProfileStatus = 'Unavailable';
const availableProfileStatus = 'Available';
const incorrectProfileStatus = 'incorrect';
const profileIncorrectMessgae = '\"finalProfileStatusIndicator\" must be one of [Available, Unavailable, ]';
const noICCIDMessage = '"ICCID" is not allowed to be empty';

describe('Delete NAP Validation', function() {
  this.timeout(10000);
  describe('Delete NAP Validation', function () {
    it('TC 01: has to be a function', function() {
      expect(validate).to.be.a('function');
    });

	it('TC 02: Validation Passed (20 digit ICCID and Profile Status Available)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCID;
	  payload.query.finalProfileStatusIndicator = availableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 03: Validation Passed (20 digit ICCID with F and Profile Status Available)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCIDF;
	  payload.query.finalProfileStatusIndicator = availableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 04: Validation Passed (19 digit ICCID and Profile Status Available)', function () {
      const payload = validatePayload();
	  payload.params.id = ICCID;
	  payload.query.finalProfileStatusIndicator = availableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 05: Validation Passed (20 digit ICCID and Profile Status Unavailable)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCID;
	  payload.query.finalProfileStatusIndicator = unavailableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 06: Validation Passed (20 digit ICCID with F and Profile Status Unavailable)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCIDF;
	  payload.query.finalProfileStatusIndicator = unavailableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 07: Validation Passed (19 digit ICCID and Profile Status Unavailable)', function () {
      const payload = validatePayload();
	  payload.params.id = ICCID;
	  payload.query.finalProfileStatusIndicator = unavailableProfileStatus;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 08: Validation Passed (20 digit ICCID and No Profile Status)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCID;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 09: Validation Passed (20 digit ICCID with F and No Profile Status)', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCIDF;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });
	
	it('TC 10: Validation Passed (19 digit ICCID and No Profile Status)', function () {
      const payload = validatePayload();
	  payload.params.id = ICCID;
      const errObj = validate(payload);
      expect(errObj).to.be.false;
    });	
	
    it('TC 11: ICCID not matching regex', function () {
      const payload = validatePayload();
	  payload.params.id = incorrectICCID;
	  payload.query.finalProfileStatusIndicator = availableProfileStatus;
      const errObj = validate(payload);
	  expect(errObj).to.not.be.null;
      expect(errObj).to.be.equal(`\"ICCID\" with value \"${payload.params.id}\" fails to match the required pattern: /^[1-9][0-9]{18}[0-9F]?$/`);
    });
	
	it('TC 12: Profile Status not matching', function () {
      const payload = validatePayload();
	  payload.params.id = correctICCID;
	  payload.query.finalProfileStatusIndicator = incorrectProfileStatus;
      const errObj = validate(payload);
	  expect(errObj).to.not.be.null;
      expect(errObj).to.be.equal(profileIncorrectMessgae);
    });
	
	it('TC 13: No ICCID', function () {
      const payload = validatePayload();
	  payload.query.finalProfileStatusIndicator = incorrectProfileStatus;
      const errObj = validate(payload);
	  expect(errObj).to.not.be.null;
      expect(errObj).to.be.equal(noICCIDMessage);
    });
  });
});