'use strict';

const expect = require('chai').expect;
const validation = require('../../source/es2/get-history/validation');

describe('Get-History Validation', function() {
  this.timeout(10000);
  describe('ICCID search Validation', function () {
    it('has to be a function', function() {
      expect(validation.validateIccidSchema).to.be.a('function');
    });

    it('empty object is passed', function () {
      const errObj = validation.validateIccidSchema({});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" is required');
    });

    it('id field is not passed', function () {
      const errObj = validation.validateIccidSchema({ msisdn: '660000150000124408'});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" is required');
    });

    it('unidentified field is not passed', function () {
      const errObj = validation.validateIccidSchema({ id: '66000015000012440812', msisdn: '660000150000124408'});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"msisdn" is not allowed');
    });

    it('ICCID length less than 20', function () {
      const errObj = validation.validateIccidSchema({ id: '660000150000124408' });
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" with value "660000150000124408" fails to match the required pattern: /^[1-9][0-9]{18}[0-9F]?$/');
    });

    it('ICCID length greater than 20', function () {
      const errObj = validation.validateIccidSchema({ id: '660000150000121244081' });
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" with value "660000150000121244081" fails to match the required pattern: /^[1-9][0-9]{18}[0-9F]?$/');
    });

    it('Negetive number is passed', function () {
      const errObj = validation.validateIccidSchema({ id: '-66000015000012124408' });
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" with value "-66000015000012124408" fails to match the required pattern: /^[1-9][0-9]{18}[0-9F]?$/');
    });

    it('Hexadecimal number is passed', function () {
      const errObj = validation.validateIccidSchema({ id: '66000015000F12124D08' });
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"ICCID" with value "66000015000F12124D08" fails to match the required pattern: /^[1-9][0-9]{18}[0-9F]?$/');
    });

    it('19 digit is passed', function () {
      const errObj = validation.validateIccidSchema({ id: '6600001500012124085' });
      expect(errObj).to.be.null;
    });

    it('19 digit with \'F\' is passed', function () {
      const errObj = validation.validateIccidSchema({ id: '6600001500012124084F' });
      expect(errObj).to.be.null;
    });

    it('20 digit is passed', function () {
      const errObj = validation.validateIccidSchema({ id: '66000015000121240812' });
      expect(errObj).to.be.null;
    });
  });
  describe('MSISDN search Validation', function () {
    it('has to be a function', function() {
      expect(validation.validateMsisdnSchema).to.be.a('function');
    });

    it('empty object is passed', function () {
      const errObj = validation.validateMsisdnSchema({});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"MSISDN" is required');
    });

    it('msisdn field is not passed', function () {
      const errObj = validation.validateMsisdnSchema({ id: '660000150000124408'});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"MSISDN" is required');
    });

    it('unidentified field is not passed', function () {
      const errObj = validation.validateMsisdnSchema({ id: '66000015000012440812', msisdn: '660000150000124408'});
      expect(errObj).to.not.be.null;
      expect(errObj[0].message).to.be.equal('"id" is not allowed');
    });

    it('MSISDN is passed', function () {
      const errObj = validation.validateMsisdnSchema({ msisdn: '44345678901234' });
      expect(errObj).to.be.null;
    });
  });
});