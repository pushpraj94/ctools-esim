'use strict';

const expect = require('chai').expect;
const isValid = require('../../').es2.handleDownloadProgress.isValid;
const _ = require('lodash');
const config = require('../../source/es2/handleDownloadProgress/config.js');

const es2Req = {
  header: {
    functionRequesterIdentifier: 'dafsasdfasd',
    functionCallIdentifier: 'Notification',
  },
  eid: '00636856303030303030303037303800',
  iccid: '12345678901234567890',
  profileType: 'Prepaid',
  timestamp: '2017-03-01T11:06:18Z',
  notificationPointId: '3',
  notificationPointStatus: {
    status: 'Executed-Success',
    statusCodeData: {
      subjectCode: '',
      reasonCode: '',
      subjectIdentifier: '',
      message:'',
    },
  },
};

describe('HandleDownloadProgress validation test suite', function() {
  it('has an isValid function', function() {
    expect(isValid).to.be.a('function');
  });

  it('a valid payload has no errors', function() {
    const result = isValid(es2Req);
    expect(result).to.be.equal(true);
  });

  it('iccid is required', function() {
    const result = isValid(_.omit(es2Req, 'iccid'));
    expect(result).to.be.equal(false);
  });

  it('iccid must be valid', function() {
    const result = isValid(_.merge({}, es2Req, {
      iccid: '02345678901234567890'
    }));
    expect(result).to.be.equal(false);
  });

  _.each(config.ALLOWED_VALUES.POINT_ID, (x) => {
    _.each(x.ERROR, (codes, subjectCode) => {
      _.each(codes, (reasonCode) => {
        it(`valid failed scenarion pointId: ${x.KEY} subjectCode: ${subjectCode} reasonCode: ${reasonCode}`, () => {
          const result = isValid(_.merge({}, es2Req, {
            notificationPointId: x.KEY,
            notificationPointStatus: {
              status: 'Failed',
              statusCodeData: {
                subjectCode: subjectCode,
                reasonCode: reasonCode,
              },
            },
          }));
          expect(result).to.be.equal(true);
        })
      });
    });
  });

  it('not allowed error codes are invalid', function() {
    const result = isValid(_.extend(es2Req, {
      notificationPointId: '1',
      notificationPointStatus: {
        status: 'Failed',
        statusCodeData: {
          subjectCode: '8.1.1',
          reasonCode: '3.9',
        },
      },
    }));
    expect(result).to.be.equal(false);
  });

  it('not failed notification should not have subjectCode or reasonCode', function() {
    const result = isValid(_.extend(es2Req, {
      notificationPointStatus: {
        status: 'Executed-Success',
        statusCodeData: {
          subjectCode: '8.1.1',
          reasonCode: '',
        },
      },
    }));
    expect(result).to.be.equal(false);

    const result2 = isValid(_.extend(es2Req, {
      notificationPointStatus: {
        status: 'Executed-Success',
        statusCodeData: {
          subjectCode: '',
          reasonCode: '4',
        },
      },
    }));
    expect(result2).to.be.equal(false);
  });
});
