'use strict';

const expect = require('chai').expect;
const extract = require('../../source/es2/smdp/extract').getES2Status;
const executionStatus = require('./payloads').executionStatus;

const status = ['Executed-Success', 'Failed'];
const subjectCode = ['8.2.1', '8.2.6'];
const reasonCode = ['1.2', '3.3', '3.9', '3.10', '3.12', '3.23'];
const message = ['Unknown error', 'Unknown profile', 'Profile authorization failed', 'Profile already in use', 'Profile and EID association is invalid', 'Different matchingID is associated with ICCID'];
const result = {};


describe('SMDP Extract function', function() {
  this.timeout(10000);
  describe('SMDP Extract function', function () {
    it('TC 01: has to be a function', function() {
      expect(extract).to.be.a('function');
    });
	
	it(`TC 02: ${message[0]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[4];
	  payload.statusCodeData.message = message[0];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 03: ${status[1]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[5];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 04: ${message[1]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[2];
	  payload.statusCodeData.message = message[1];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 05: ${message[2]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[0];
	  payload.statusCodeData.message = message[2];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 06: ${message[3]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[1];
	  payload.statusCodeData.message = message[3];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 07: ${message[4]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[3];
	  payload.statusCodeData.message = message[4];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 08: ${message[5]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
	  payload.statusCodeData.subjectCode = subjectCode[0];
	  payload.statusCodeData.reasonCode = reasonCode[3];
	  payload.statusCodeData.message = message[5];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
	
	it(`TC 09: ${status[0]}`, function () {
      const payload = executionStatus();
	  payload.status = status[0];
      const errObj = extract(payload);
      expect(errObj).to.be.result;
    });
  });
});