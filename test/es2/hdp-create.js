'use strict';

const expect = require('chai').expect;
const create = require('../../').es2.handleDownloadProgress.create;
const op = require('object-path');
const _ = require('lodash');

const requesterId = 'some_requesterId_value';
const functionCallIdentifier = 'some_functionCallIdentifier_value';
const eid = 'some_eid_value';
const iccid = 'some_iccid_value';
const profileType = 'some_profileType_value';
const timestamp = 'some_timestamp_value';
const notificationPointId = 'some_notificationPointId_value';
const notificationPointStatus = 'some_notificationPointStatus_value';
const resultData = 'some_resultData_value';
const subjectCode = 'some_subjectCode_value';
const reasonCode = 'some_reasonCode_value';
const subjectIdentifier = 'some_subjectIdentifier_value';
const message = 'some_message_value';

const es2Req = {
  header: {
    functionRequesterIdentifier: requesterId,
    functionCallIdentifier: functionCallIdentifier,
  },
  eid,
  iccid,
  profileType,
  timestamp,
  notificationPointId,
  notificationPointStatus: {
    status: notificationPointStatus,
    statusCodeData: {
      subjectCode: subjectCode,
      reasonCode: reasonCode,
      subjectIdentifier: subjectIdentifier,
      message: message,
    },
  },
  resultData,
};

describe('HandleDownloadProgress create request test suite', function() {
  it('has an create function', function() {
    expect(create).to.be.a('function');
  });

  it('creates a handleDownloadProgress request', function() {
    const req = create({
      requesterId,
      functionCallIdentifier,
      iccid,
      eid,
      timestamp,
      notificationPointId,
      notificationPointStatus,
      profileType,
      resultData,
      subjectCode,
      reasonCode,
      subjectIdentifier,
      message,
    });

    expect(req).to.be.eql(es2Req);
  });
});
