'use strict';

const expect = require('chai').expect;
const extractRequestData = require('../../').es2.handleDownloadProgress.extractRequestData;

const requesterId = 'some_requesterId_value';
const functionCallIdentifier = 'some_functionCallIdentifier_value';
const eid = 'some_eid_value';
const iccid = 'some_iccid_value';
const profileType = 'some_profileType_value';
const timestamp = 'some_timestamp_value';
const notificationPointId = 'some_notificationPointId_value';
const notificationPointStatus = 'some_notificationPointStatus_value';
const resultData = 'some_resultData_value';
const subjectCode = 'some_subjectCode_value';
const reasonCode = 'some_reasonCode_value';
const subjectIdentifier = 'some_subjectIdentifier_value';
const message = 'some_message_value';

const es2Req = {
  header: {
    functionRequesterIdentifier: requesterId,
    functionCallIdentifier: functionCallIdentifier,
  },
  eid,
  iccid,
  profileType,
  timestamp,
  notificationPointId,
  notificationPointStatus: {
    status: notificationPointStatus,
    statusCodeData: {
      subjectCode: subjectCode,
      reasonCode: reasonCode,
      subjectIdentifier: subjectIdentifier,
      message: message,
    },
  },
  resultData,
};

const outObj = {
  requesterId,
  functionCallIdentifier,
  eid,
  iccid,
  profileType,
  timestamp,
  notificationPointId,
  notificationPointStatus,
  resultData,
  subjectCode,
  reasonCode,
  subjectIdentifier,
  message,
}

describe('HandleDownloadProgress extractRequestData request test suite', function() {
  it('has an extractRequestData function', function() {
    expect(extractRequestData).to.be.a('function');
  });

  it('extracts the data correctly', function() {
    const data = extractRequestData(es2Req);
    expect(data).to.be.eql(outObj);
  });
});
