'use strict';

const expect = require('chai').expect;
const transform = require('../../source/es2/delete/transform').transform;


describe('Delete NAP Transform', function() {
  this.timeout(10000);
  describe('Delete NAP Transform', function () {
    it('TC 01: has to be a function', function() {
      expect(transform).to.be.a('function');
    });
  });
});