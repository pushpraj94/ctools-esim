'use strict';

function validatePayload() {
  return {
	params: { id: '' },
    query: { finalProfileStatusIndicator: '' }
  };
}

function executionStatus() {
  return {
    status: '',
    statusCodeData: { subjectCode: '',
      reasonCode: '',
      message: '' }
  };
}

module.exports = {
  validatePayload,
  executionStatus,
};
