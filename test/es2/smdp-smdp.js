'use strict';

const expect = require('chai').expect;
const smdpConstructor = require('../../source/es2/smdp');
const smdp = require('../../source/es2/smdp/smdp');
const smdpDetails = { SMDP_ID: 1,
  PRIVATE_SMDP_ADDRESS: null,
  PUBLIC_SMDP_FQDN: 'smdpA',
  SMDS_ADDRESS: null,
  NOTIFICATION_REQUESTER_IDENTIFIER: 'dummyIdentity',
  SMDP_NAME: 'testSM-DP',
  AUTHENTICATION_TYPE: 'basicAuthentication',
  SSL_REQUIRED: '1',
  USER_NAME: null,
  USER_PASSWORD: null,
  SMDP_CA: 'cert_CA',
  SMDP_CRT: 'cert_CRT',
  SMDP_KEY: 'cert_KEY' };
const smdpObj = new smdp(smdpDetails, '1');
const profileDetails = { ICCID: '1123456789012356789F',
  MSISDN: null,
  EID: null,
  IMEI: null,
  DEVICE_TYPE: 'Tablet',
  SMDP_ID: '1',
  MATCHING_ID: 'dummyMatch',
  PROFILE_STATUS_ID: '3',
  OPERATOR_ID: '1',
  SMDP_ADDRESS: 'smdpA' };
const result = {};
const smdpURL = '/gsma/rsp2/es2plus/cancelOrder';
const SMDPIdentifier = 'fctCallId';
const error = { code: 'ServerUnreachable' };
const response = {};

describe('SMDP Extract function', function() {
  this.timeout(10000);
  describe('SMDP Extract function', function () {
    it('TC 01: SMDP has to be a function', function() {
      expect(smdpConstructor.SMDP).to.be.a('function');
    });
	
	it('TC 02: getSmdpUrl has to be a function', function() {
      expect(smdpObj.getSmdpUrl).to.be.a('function');
    });
	
	it('TC 03: cancelOrderCall has to be a function', function() {
      expect(smdpObj.cancelOrderCall).to.be.a('function');
    });
	
	it('TC 04: basicAuthenticationSmdpClientFactory has to be a function', function() {
      expect(smdpObj.basicAuthenticationSmdpClientFactory).to.be.a('function');
    });
	
	it('TC 05: noAuthenticationSmdpClientFactory has to be a function', function() {
      expect(smdpObj.noAuthenticationSmdpClientFactory).to.be.a('function');
    });
	
	it('TC 06: smdpClientAddSSL has to be a function', function() {
      expect(smdpObj.smdpClientAddSSL).to.be.a('function');
    });
	
	it('TC 07: callSMDP has to be a function', function() {
      expect(smdpObj.callSMDP).to.be.a('function');
	});
	  
	it('TC 08: validateES2Response has to be a function', function() {
      expect(smdpObj.validateES2Response).to.be.a('function');
	});
	
	it('TC 09: Cancel order call function', function () {
      const errObj = smdpObj.cancelOrderCall(profileDetails, 'Available');
      expect(errObj).to.be.result;
    });
	
	it('TC 10: getSmdpUrl function', function () {
      const errObj = smdpObj.getSmdpUrl(smdpURL);
      expect(errObj).to.be.result;
    });
	
	it('TC 11: basicAuthenticationSmdpClientFactory function', function () {
      const errObj = smdpObj.basicAuthenticationSmdpClientFactory(smdpURL);
      expect(errObj).to.be.result;
    });
	
	it('TC 12: noAuthenticationSmdpClientFactory function', function () {
      const errObj = smdpObj.noAuthenticationSmdpClientFactory(smdpURL);
      expect(errObj).to.be.result;
    });
	
	it('TC 13: smdpClientAddSSL function', function () {
      const errObj = smdpObj.smdpClientAddSSL(SMDPIdentifier);
      expect(errObj).to.be.result;
    });
	
	it('TC 14: callSMDP function', function () {
      const errObj = smdpObj.callSMDP();
      expect(errObj).to.be.result;
    });
	
	it('TC 15: validateES2Response function', function () {
      const errObj = smdpObj.validateES2Response(error, response);
      expect(errObj).to.be.result;
    });
  });
});